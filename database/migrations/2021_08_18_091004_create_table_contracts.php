<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->id();
            $table->integer("file_id")->nullable();
            $table->string("idcontrato")->nullable();
            $table->string("nAnuncio")->nullable();
            $table->text("tipoContrato")->nullable();
            $table->string("tipoprocedimento")->nullable();
            $table->text("objectoContrato")->nullable();
            $table->text("adjudicantes")->nullable();
            $table->text("adjudicatarios")->nullable();
            $table->string("dataPublicacao")->nullable();
            $table->string("dataCelebracaoContrato")->nullable();
            $table->string("precoContratual")->nullable();
            $table->text("cpv")->nullable();
            $table->string("prazoExecucao")->nullable();
            $table->text("localExecucao")->nullable();
            $table->text("fundamentacao")->nullable();
            $table->boolean("read")->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
