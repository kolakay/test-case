<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileController;
use App\Http\Controllers\ContractController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::post('/file/store', [FileController::class, 'store'])->name('file.store');
Route::get('/file/upload-status/{id}', [FileController::class, 'uploadStatus'])->name('file.upload-status');

Route::post('/contract/search-data', [ContractController::class, 'queryData'])->name('file.query-data');
Route::get('/contract/get/{idcontrato}', [ContractController::class, 'getContractData'])->name('contract.get-data');
Route::get('/contract/read-status/{idcontrato}', [ContractController::class, 'getReadStatus'])->name('contract.read-status');




