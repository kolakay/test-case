##Description  
A REST API-based application that allows users to upload and then search and consume data.   
For this task, portuguese public tender data was used but the task was valid with any other dataset.
Dataset used for testing will be this or a subset of this but will always include the heading row and  
the same number of columns and naming/format (what might change is the number of rows):  
https://www.dropbox.com/s/oy9ysebchcis1uq/contratos2016.xlsx?dl=0  

## Project Requirements
PHP 7.3+
Composer
Access to CLI

## Installation

Unzip project into youre prefered location.  

Using CLI cd into project directory( where project was unzipped)  

Install dependecies using ### `composer install`  

Create a new file .env  

Copy contents in .env.example into .env  
Update your database details in .env  

run `php artisan key:generate` to generate new app key  
run `php artisan migrate` to migrate tables   
run `php artisan scribe:generate` to generate documentation  
run `php artisan serve` to startup the application  

setup cron  
`* * * * * cd /path-to-test-task && php artisan schedule:run >> /dev/null 2>&1`

Visit http://localhost:8000 in your browser to use applicaiton  
