<?php

namespace App\Imports;

use App\Models\FileUploadModel;
use App\Models\ContractModel;
use \PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class ContractImport implements ToCollection, WithHeadingRow
{
    private $file;
    /**
    * @param FileUploadModel $file
    */
    public function __construct(FileUploadModel $file){
        $this->file = $file;
    }

    /**
    * @param array $row
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            $contract = ContractModel::firstOrNew([
                'idcontrato' => $row['idcontrato']
            ]);
            $datapublicacao = intval($row['datapublicacao']);
            $datacelebracaocontrato = intval($row['datacelebracaocontrato']);
            $formatedDatapublicacao= Date::excelToDateTimeObject($datapublicacao)->format('m/d/Y');
            $formatedDatacelebracaocontrato= Date::excelToDateTimeObject($datacelebracaocontrato)->format('m/d/Y');

            $contract->file_id = $this->file->id;
            $contract->idcontrato = $row['idcontrato'];

            $contract->nAnuncio = $row['nanuncio'];
            $contract->tipoContrato = $row['tipocontrato'];
            $contract->tipoprocedimento = $row['tipoprocedimento'];
            $contract->objectoContrato = $row['objectocontrato'];
            $contract->adjudicantes = $row['adjudicantes'];
            $contract->adjudicatarios = $row['adjudicatarios'];
            $contract->dataPublicacao = $formatedDatapublicacao;
            $contract->dataCelebracaoContrato = $formatedDatacelebracaocontrato;
            $contract->precoContratual = $row['precocontratual'];
            $contract->cpv = $row['cpv'];
            $contract->prazoExecucao = $row['prazoexecucao'];
            $contract->localExecucao = $row['localexecucao'];
            $contract->fundamentacao = $row['fundamentacao'];
            $contract->save();

            // \Log::info($contract->id);

        }
    }
}
