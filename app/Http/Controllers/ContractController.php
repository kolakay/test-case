<?php

namespace App\Http\Controllers;

use App\Models\ContractModel;
use Illuminate\Http\Request;
use Log, Exception;

class ContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('secured');

    }

    /**
     *  Query/search data from the database based on: 
     * date (dataCelebracaoContrato in xls), 
     * amount (range) (precoContratual in xls), 
     * winning company (adjudicatarios in the xls)
     *
     * This endpoint allows the query/search data in the xls from the database.
     * 
     * @header Authorization Bearer {Token}
     * 
     * @bodyParam filter_by string required The type of filter-- could be date,amount, company. Example: company
     * @bodyParam date string The dataCelebracaoContrato in xls, required if filter_by = date. Example 05/30/2016
     * @bodyParam start_amount integer The start range precoContratual in xls, needed if filter_by = amount. Example: 4206
     * @bodyParam end_amount integer The end range precoContratual in xls, needed if filter_by = amount. Example: 461069.28
     * @bodyParam company string The adjudicatarios in xls, needed if filter_by = company. Example: 502496878 - CONSTRUÇÕES PRAGOSA, S.A.
     * 
     * @response {
     *  "id": 4,
     *  "file_id": 1,
     *  "idcontrato": 4008909
     * }
     * @response status=503 scenario="Database not setup" {
     *  "error": true,
     *  "uploadId": "",
     *  "message": "Unable to complete request. Please try again later"
     * }
    */
    public function queryData(Request $request){
        try{
            $result = [];
            switch ($request->filter_by) {
                case 'date':
                    $result = ContractModel::where('dataCelebracaoContrato', 'like', '%' . $request->date . '%')
                        ->get();
                    break;
                case 'amount':
                    $result = ContractModel::whereBetween('precocontratual', [$request->start_amount, $request->end_amount])
                        ->get();
                    break;
                
                default:
                    $result = ContractModel::where('adjudicatarios', 'like', '%' . $request->company . '%')
                        ->get();
                    break;
            }

            return response()->json([
                'result' => $result
            ]);

        }catch(Exception $error){
            Log::info('ContractController@getReadStatus error message: ' . $error->getMessage());

            return response()->json([
                'error' => true,
                'message' => 'Unable to complete request. Please try again later'
            ], 503);
        }
    }

    /**  
     * Get all data for a given contract as long as provided with an ID 
     * This endpoint exists to get all data for a given contract as long as provided with an ID. 
     * This marks row as ‘read’.
     *  
     * @header Authorization Bearer {Token}
     * @urlParam ID integer required The ID of the Contract.
     * 
     * @response scenario=success {
     *  "error": false,
     *  "fileName": "example",
     *  "status": 'pending'
     * }
     * 
     * @response status=404 scenario="Contract not found" {
     *  "error": true,
     *  "message": "Contract not found"
     * } 
     * 
     * @response status=503 scenario="Database not setup" {
     *  "error": true,
     *  "uploadId": "",
     *  "message": "Unable to complete request. Please try again later"
     * }
     */
    public function getContractData($contractID){
        try{
            $contract = ContractModel::where('idcontrato', $contractID)->first();
            if(!$contract){
                return response()->json([
                    'error' => true,
                    'message' => 'Contract not found'
                ], 404);
            }

            $contract->read = true;
            $contract->save();
            return response()->json([
                'error' => false,
                "contract" => $contract->makeHidden(['read']),
            ]);

        }catch(Exception $error){
            Log::info('ContractController@getContractData error message: ' . $error->getMessage());

            return response()->json([
                'error' => true,
                'message' => 'Unable to complete request. Please try again later'
            ], 503);
        }
    }

    /** 
     * Check if a certain contract (row) was ever read or not
     * 
     * This endpoint exists to check if a certain contract (row) was ever read or not
     * @header Authorization Bearer {Token}
     * 
     * @urlParam ID integer required The ID of the Contract.
     * 
     * @response scenario=success {
     *  "error": false,
     *  "fileName": "example",
     *  "status': 'pending'
     * }
     * 
     * @response status=404 scenario="Contract not found" {
     *  "error": true,
     *  "message": "Contract not found"
     * } 
     * 
     * @response status=503 scenario="Database not setup" {
     *  "error": true,
     *  "uploadId": "",
     *  "message": "Unable to complete request. Please try again later"
     * }
     */
    public function getReadStatus($contractID){
        try{
            $contract = ContractModel::where('idcontrato', $contractID)->first();
            if(!$contract){
                return response()->json([
                    'error' => true,
                    'message' => 'Contract not found'
                ], 404);
            }

            return response()->json([
                'error' => false,
                "read" => $contract->read,
            ]);

        }catch(Exception $error){
            Log::info('ContractController@getReadStatus error message: ' . $error->getMessage());

            return response()->json([
                'error' => true,
                'message' => 'Unable to complete request. Please try again later'
            ], 503);
        }
    }
}
