<?php

namespace App\Http\Controllers;

use App\Helpers\Paths;
use App\Models\FileUploadModel;
use Illuminate\Http\Request;
use App\Imports\ContractImport;
use Maatwebsite\Excel\Facades\Excel;
use Log, Storage, File, Exception;

class FileController extends Controller
{

    public function __construct()
    {
        $this->middleware('secured')->except(['processUploadedFile']);

    }
    
    /**
     * Upload a .xls with data.
     *
     * This endpoint allows users to upload a .xls with data for treatment.
     * 
     * @header Content-Type multipart/form-data
     * @header Authorization Bearer {Token}

     * @bodyParam name string The xls title
     * @bodyParam contracts_file file required The xls .
     * 
     * @response scenario=success {
     *  "error": false,
     *  "uploadId":1,
     *  "message": 'File uploaded. queued for processing'
     * }
     * 
     * @response status=400 scenario="invalid file extension" {
     *  "error": true,
     *  "uploadId": "",
     *  "message": "File extension is not valid"
     * } 
     * 
     * @response status=503 scenario="Database not setup" {
     *  "error": true,
     *  "uploadId": "",
     *  "message": "Unable to complete request. Please try again later"
     * }
     */
    public function store(Request $request){
        try{
            if (request()->hasFile('contracts_file')) {
                $response = $this->storeFileUpload($request);
                if($response['error'] == true){
                    return response()->json([
                        'error' => true,
                        'uploadId' => "",
                        'message' => $response['message']
                    ], $response['code']);
                }

                $newFile = new FileUploadModel();
                $newFile->name = ($request->name) ? $request->name : $response['fileName'];
                $newFile->file_name = $response['fileName'];
                $newFile->status = 'pending';
                $newFile->save();
                
                
                return response()->json([
                    'error' => false,
                    'uploadId' => $newFile->id,
                    'message' => 'File uploaded. queued for processing'
                ]);
            
            }
            return response()->json([
                'error' => true,
                'uploadId' => "",
                'message' => 'Unable to complete request. Please upload file'
            ], 400);

        }catch(Exception $error){
            Log::info('FileController@store error message: ' . $error->getMessage());

            return response()->json([
                'error' => true,
                'name' => "",
                'message' => 'Unable to complete request. Please try again later'
            ], 503);
        }
    }

    private function storeFileUpload($request){
        try{

            $file = $request->file('contracts_file');
            $fileExtension = $file->getClientOriginalExtension();
            if(!in_array($fileExtension, $this->getFileExtensions())){
                return [
                    'error' => true,
                    'fileName' => "",
                    'code' => 400,
                    'message' => "File extension is not valid"
                ];
            }
            
            $fileName = str_replace(' ', '_',$file->getClientOriginalName());
            $prevFile = FileUploadModel::where('file_name', $fileName)->first();
            if($prevFile){
                return [
                    'error' => true,
                    'fileName' => "",
                    'code' => 400,
                    'message' => "file already exists"
                ];
            }
            $filePath = Paths::DOCUMENT_PATH. $fileName;
            Storage::put($filePath, File::get($file));

            return [
                'error' => false,
                'fileName' => $fileName
            ];

        }catch(Exception $error){
            Log::info('FileController@storeFileUpload error message: ' . $error->getMessage());

            return [
                'error' => true,
                'fileName' => "",
                'code' => 500,
                'message' => 'Encountered an error while storing the file. Please try again later'
            ];
        }
       
    }

    private function getFileExtensions(){
        return [
            'xls',
            'xlsx',
        ];
    }

    public function processUploadedFile(){
        $file = FileUploadModel::where('status', 'pending')->first();
        if($file){
            $file->status = 'processing';
            $file->save();
    
            $response = $this->runProcess($file);
            if(!$response['error']){
                $file->status = 'processed';
                $file->save();
            }else{
                $file->status = 'failed';
                $file->save();
            }
        }
        

    }

    private function runProcess(FileUploadModel $file){
        try {
            
            $filePath = Paths::DOCUMENT_PATH. $file->file_name;
            
            Excel::import(new ContractImport($file), $filePath);

            return [
                'error' => false,
                'status' => 'processed'
            ];
        } catch (Exception $error) {
            Log::info('FileController@runProcess error message: ' . $error->getMessage());
            
            return [
                'error' => true,
                'status' => 'failed'
            ];
        }
       
    }

    /**  
     * Query the processing status of the uploaded xls
     *
     * This endpoint exist to query the status of processing the uploaded xls and adding it to the database.
     * 
     * @urlParam id integer required The ID of the uploaded file.
     * @header Authorization Bearer {Token}
     * 
     * @response scenario=success {
     *  "error": false,
     *  "fileName": "example",
     *  "status': 'pending'
     * }
     * 
     * @response status=404 scenario="uploaded file not found" {
     *  "error": true,
     *  "message": "Uploaded file not found"
     * } 
     * 
     * @response status=503 scenario="Database not setup" {
     *  "error": true,
     *  "uploadId": "",
     *  "message": "Unable to complete request. Please try again later"
     * }
     */
    public function uploadStatus($fileId){
        try{
            $file = FileUploadModel::where('id', $fileId)->first();
            if(!$file){
                return response()->json([
                    'error' => true,
                    'message' => 'Uploaded file not found'
                ], 404);
            }

            return response()->json([
                'error' => false,
                "fileName" => $file->name,
                'status' => $file->status
            ]);

        }catch(Exception $error){
            Log::info('FileController@uploadStatus error message: ' . $error->getMessage());

            return response()->json([
                'error' => true,
                'message' => 'Unable to complete request. Please try again later'
            ], 503);
        }
    }

    
}
