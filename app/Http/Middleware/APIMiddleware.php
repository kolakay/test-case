<?php

namespace App\Http\Middleware;

use Closure;

class APIMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = '8f9ca25f-a7d5-45c6-8de9-612490abdadd';
        $requestToken = $request->bearerToken();
        if($token != $requestToken){
            return response()->json([
                "error" => true,
                "message" => "Auth Token is needed to proceed"
            ], 401);
        }

        return $next($request);
    }
}
